
import route
import socket


def send_answer(conn, status="200 OK", typ="text/plain; charset=utf-8", data=""):
  data = data.encode("utf-8")
  conn.send(b"HTTP/1.1 " + status.encode("utf-8") + b"\r\n")
  conn.send(b"Content-Type: " + typ.encode("utf-8") + b"\r\n")
  conn.send(b"Content-Length: " + bytes(len(data)) + b"\r\n")
  conn.send(b"\r\n")
  conn.send(data)

def parse(conn, addr):
  data = b""
  while not b"\r\n" in data:
    tmp = conn.recv(1024)
    if not tmp:
      break
    else:
      data += tmp

    if not data:
      return
    udata = data.decode("utf-8")
    udata = udata.split("\r\n", 1)[0]
    method, address, protocol = udata.split(" ", 2)
    #print(data)
    send_route = route.Route(method, address, data)
    server_data = send_route.routes()
    type_ans = "text/html; charset=utf-8"

    send_answer(conn, typ=type_ans, data=server_data)


serv_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serv_sock.bind(('', 8080))
serv_sock.listen(10)


try:
  while 1:
    client_sock, client_addr = serv_sock.accept()
    print('Connected by ', client_addr[0])
    try:
      parse(client_sock, client_addr)
    except:
      send_answer(client_sock, "500 Internal Server Error", data="Ошибка")
    finally:
      client_sock.close()
finally: serv_sock.close()


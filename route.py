import storage
import pageHTML

class Route:

  def __init__(self, method, url, data):
    self.method = method
    self.url = url
    self.data = data

  def routes(self):
    init_page_html = pageHTML.Shablone()
    if self.method == "GET" and self.url == "/" or self.url == "/log_out":
      return init_page_html.home_page()
    elif self.method == "GET" and self.url == "/sign_up":
      return init_page_html.sigin_up_page()
    elif self.method == "GET" and self.url == "/log_in":
      return init_page_html.log_in_page()
    elif self.method == "POST" and self.url == "/accaunts":
      parse_data = self.parse_data_form()
      print(parse_data)
      init_class_storage = storage.Storage(parse_data)

      if init_class_storage.log_in(parse_data) == "LOG IN":   # log_in() - метод проверяет существует ли пользователь в базе данных
        return init_page_html.accaunts() # если да, то верни такую страницу
      else:
        print('No DB!')
        return init_page_html.home_page() # если нет, то страница вернётся домашняя

    elif self.method == "POST" and self.url == "/save":
      parse_data = self.parse_data_form()
      #print(parse_data)
      init_class_storage = storage.Storage(parse_data)
      init_class_storage.save_in_db()

      return init_page_html.save_user()

  def say_method(self):
    print('Method = ', self.method)

  def parse_data_form(self):
    req = self.data.decode("utf-8")
    headers, entity_body = req.split('\r\n\r\n')
    value = entity_body.split('&')
    #print(value)
    user = {}
    for dt in value:
      key, value = dt.split('=')
      for sym in value:
        if sym == '+':
          q = value.split('+')
          value = ' '.join(q)
      user.update({key: value})
    #print(user)
    return user

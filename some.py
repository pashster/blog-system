import re
import storage


VAR_TOKEN_START = '{{'
VAR_TOKEN_END = '}}'
BLOCK_TOKEN_START = '{%'
BLOCK_TOKEN_END = '%}'

PATTERN_BLOK = r'{%.*?%}'
PATTERN_VAR = r'{{.*?}}'

TOK_REGEX = re.compile(r"(%s.*?%s|%s.*?%s)" % (
    VAR_TOKEN_START,
    VAR_TOKEN_END,
    BLOCK_TOKEN_START,
    BLOCK_TOKEN_END
))

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! #
#                                                                                                #
##################################################################################################
##        После первого прохождения по строке и нахождения соответствия, ОБРАБАТЫВАЕМ.          ##
## Затем отправляем полученную строку на проверку и так далее пока соответсвий не будет найдено ##
##################################################################################################
#                                                                                                #
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! #

def array_to_string_conversion(arr_html): # Преобразование массива в строку
  print('Array to string')
  string_html = ''.join(arr_html)
  return check_for_presence(string_html)

def check_for_presence(new_string): # Проверить наличие
  print('Check for parse')
  conformity = re.search(TOK_REGEX, new_string)

  if conformity:
    return regular_expression_detection(new_string)
  else:
    return sending_to_the_client(new_string)


def string_processing(html_in_array): # Обработка строк
  print("PROCESSING NOW!!!")
  for item in html_in_array:
    regular = re.search(TOK_REGEX, item)
    if regular:
      result = re.search(PATTERN_BLOK, item)
      if result:
        cmd_str = result.group(0)
        cmd = cmd_str.split(' ')[1]
        if cmd == "for":
          storage_cmd = cmd_str.split(' ')[-2]
          print('Command = FOR')
          iterator = cmd_str.split(' ')[2]
          for el in storage.DB: # Допустим DB = [{'posts': [], 'users' : []}]
            if storage_cmd in el:
              storage_cmd = el[storage_cmd]
          #print(iterator)
              html_in_array = remove_item(html_in_array, cmd_str)
              return _for(html_in_array, iterator, storage_cmd)
            else:
              print("ERROR!")
              break
        elif cmd == "endfor":
          print('Command = ENDFOR')
          html_in_array = remove_item(html_in_array, cmd_str)
          return array_to_string_conversion(html_in_array)
        elif cmd == "if":
          print('Command = IF')
          html_in_array = remove_item(html_in_array, cmd_str)
          return _if(html_in_array)
        elif cmd == "else":
          print('Command = ELSE')
          html_in_array = remove_item(html_in_array, cmd_str)
          return _else(html_in_array)
        elif cmd == "endif":
          print('Command = ENDIF')
          html_in_array = remove_item(html_in_array, cmd_str)
          return array_to_string_conversion(html_in_array)
      else:
        result_2 = regular.group()
        pat = result_2.split(' ')[1]
        html_in_array = remove_item(html_in_array, result_2)
        return array_to_string_conversion(html_in_array)

def regular_expression_detection(html_in_line):
  arr_regular = TOK_REGEX.split(html_in_line)
  return string_processing(arr_regular)


def sending_to_the_client(ready_html): # Отправка клиенту готового HTML
  print("SEND CLIENT!!!")
  return ready_html

def _for(html_in_array, command, storage_arr):
  if storage_arr != None:
    pattern = generate_pattern(command)
    arr_reg = []
    def generate_arr(html_in_array):
      new_arr_1 = []
      for element in html_in_array:
        res = re.search(pattern, element)

        if res:
          arr_reg.append(res.group())
          result_1 = res.group()
          res_split = result_1.split(' ')[1]
          key, value = res_split.split('.')

          index_first = html_in_array.index(arr_reg[0])
          index_last = html_in_array.index(arr_reg[-1])

          new_arr = html_in_array[(index_first - 1):(index_last + 2)]

      return new_arr
    result_arr = generate_arr(html_in_array)
    new_arr_1 = result_arr * len(storage_arr)
    array_to_str = ''.join(result_arr)
    arr_in_str = ''.join(html_in_array)
    ready_html_in_array = arr_in_str.split(array_to_str)
    for element in html_in_array:
        res = re.search(pattern, element)

        if res:
          arr_reg.append(res.group())
          result_1 = res.group()
          res_split = result_1.split(' ')[1]
          key, value = res_split.split('.')

          for el in new_arr_1:
            if el == result_1:
              #print(el)
              for item in storage_arr:
                index = new_arr_1.index(result_1)
                #print(value)
                some =  str(item[value])
                new_arr_1.insert(index, some)
                new_arr_1.remove(result_1)
    arr_result_to_string = ''.join(new_arr_1)
    ready_html_in_array.insert(1, arr_result_to_string)
    return array_to_string_conversion(ready_html_in_array)

def _if(html_in_array):
  return array_to_string_conversion(html_in_array)

def _else(html_in_array):
  return array_to_string_conversion(html_in_array)

def insert_item(html_in_array, index_cmd, item):
  html_in_array.insert(index_cmd, item)
  return html_in_array

def remove_item(html_in_array, item):
  html_in_array.remove(item)
  #print(html_in_array)
  return html_in_array

def generate_pattern(command):
  pat = r''
  pat += '{{ '
  pat += command
  pat += '\.\w+ }}'

  return pat
